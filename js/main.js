var app = {
	processing: false,
	edit: false,
	student: null
};

(function() {
	$(document).ready(function() {

		app.init = function() {
			app.fetchStudents();
			app.addListeners();
		};

		app.newStudent = function(target) {
			if(app.processing) {
				return;
			}
			app.processing = true;
			var age = $('#age').val();
			var name = $('#name').val();
			var enrolled = $("#enrolled").is("checked");
			if(!name || !age) {
				alert('You must provide all required fields!');
			}
			var data = {
				student: {
					name: name,
					enrolled: enrolled,
					age: age
				}
			};
			var callback = function() {
				$('#addEditModal').modal('hide');
				app.processing = false;
				app.fetchStudents();
			};
			app.ajax('http://localhost:3000/students' + (app.student ? '/' + app.student.id : ''), (app.edit ? 'PUT' : 'POST'), data, callback);
		};

		app.addListeners = function() {
			$(".delete-student").click(function(e) {
			    e.preventDefault();
			    app.deleteStudent(e);
			});
			$('.submit-form').click(function(e) {
				e.preventDefault();
				app.newStudent(e.target);
			});
			$('.add-student').click(function(e) {
				e.preventDefault();
				app.edit = false;
			});
			$('.edit-student').click(function(e) {
				e.preventDefault();
				app.fetchStudent(e.target);
			});
		};

		app.editStudent = function(student) {
			app.edit = true;
			app.student = student;
			$('#addEditModal').modal('show');
			$('#age').val(student.age);
			$('#name').val(student.name);
			$('#enrolled').val(student.enrolled);
		};

		app.fetchStudent = function(target) {
			var student = target.attributes.student.value;
			app.ajax('http://localhost:3000/students/' + student, 'GET', null, function(student) {
				app.editStudent(student);
			});
		};

		app.fetchStudents = function() {
			app.processing = true;
			app.ajax('http://localhost:3000/students.json', 'GET', null, app.renderStudents);
		};

		app.deleteStudent = function(status) {
			var studentID = parseInt(status.target.attributes.student.value, 10);
			bootbox.confirm("Delete Student: " + studentID + '?', function(result) {
				if(!result) {
					return;
				}
				app.ajax(
					'http://localhost:3000/students/' + studentID,
					'DELETE',
					null,
					app.fetchStudents
				);
			});
		},

		app.ajax = function(url, type, data, success) {
			$.ajax({
			  url: url,
			  type: type,
			  data: data,
			  cache: false,
			  success: function(data, status) {
			  	success(data);
			  },
			  error: function(data) {
			  	if([200, 201].indexOf(data.status) != -1) {
			  		success(JSON.parse(data.responseText));
			  		return;
			  	}
			  },
			  dataType: 'application/json'
			});
		};

		app.renderStudents = function(students) {
			var renderList = [];
			for (var i = students.length - 1; i >= 0; i--) {
				renderList.push(''.concat(
					'<li class="list-group-item student">',
						'<b>Name:</b> ' + students[i].name + ' ',
						'<b>Enrolled:</b> ' + students[i].enrolled + ' ',
						'<b>Age:</b> ' + students[i].age + ' <br /><br />',
						'<div class="right">',
							'<button type="button" student="'+ students[i].id +'" class="btn align-right btn-danger delete-student">Delete</button>&nbsp;',
							'<button type="button" student="'+ students[i].id +'" class="btn align-right btn-info edit-student">Edit</button>',
						'</div>',
					'</li>'
				));
 			};
 			$('.students').html(renderList);
 			app.processing = false;
 			app.addListeners();
		};

		app.init();
	});

})();	
